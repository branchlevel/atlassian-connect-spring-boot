package com.atlassian.connect.spring.internal.lifecycle;

import javax.validation.constraints.NotNull;

/**
 * The HTTP POST body of Atlassian Connect add-on lifecycle events.
 */
public class LifecycleEvent {

    @NotNull
    public String eventType;

    @NotNull
    public String key;

    @NotNull
    public String clientKey;

    public String publicKey;

    public String sharedSecret;

    @NotNull
    public String baseUrl;

    @NotNull
    public String productType;

    // JIRA & Confluence-specific fields

    public String serverVersion;

    public String pluginsVersion;

    public String description;

    public String serviceEntitlementNumber;

    // Bitbucket-specific fields

    public String baseApiUrl;

    public OAuth2Consumer consumer;

    public BitbucketPrincipal principal;

    public BitbucketPrincipal user;

    public static class OAuth2Consumer {

        private String id;

        private String key;

        private String secret;
    }

    public static class BitbucketPrincipal {

        private String type;

        private String username;
    }
}
