package com.atlassian.connect.spring.it;

import com.atlassian.connect.spring.AtlassianHostUser;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class WebhookController {

    @RequestMapping(value = "/webhook", method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void onWebhook(@RequestBody String body, @AuthenticationPrincipal AtlassianHostUser hostUser) {
        LoggerFactory.getLogger(WebhookController.class).info(body);
    }
}
