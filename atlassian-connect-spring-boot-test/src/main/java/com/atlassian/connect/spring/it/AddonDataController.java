package com.atlassian.connect.spring.it;

import com.atlassian.connect.spring.IgnoreJwt;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class AddonDataController {

    private static final String ADDONS_PATH = "/rest/atlassian-connect/1/addons/";

    @Value("${add-on.key}")
    private String addonKey;

    @Autowired
    private RestTemplate restTemplate;

    @IgnoreJwt
    @RequestMapping(value = "/addon-public", method = GET, produces = "application/json")
    @ResponseBody
    public String getAddonPublic() {
        return restTemplate.getForEntity("http://localhost:2990/jira" + ADDONS_PATH + addonKey, String.class).getBody();
    }

    @RequestMapping(value = "/addon", method = GET)
    public String getAddon(Model model, HttpServletRequest request) throws IOException {
        String accountJson = prettyPrintJson(restTemplate.getForEntity("https://api.bitbucket.org/1.0/users/epehrson", String.class).getBody());
        model.addAttribute("addon", accountJson);
        return "addon";
    }

    private String prettyPrintJson(String addon) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object json = mapper.readValue(addon, Object.class);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
    }
}
